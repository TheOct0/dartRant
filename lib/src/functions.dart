import "package:http/http.dart" as http;
import "classes.dart";
import "dart:async";
import "dart:convert";

const String apiUrl = "https://devrant.com/api/";
const int appId = 3;

Future apiGet(String endpoint, [Map<String, String> args]) async {
  String argString = "?app=3";
  if (args.runtimeType != Null) Map<String, String> args = {};
  List arguments = [];
  if (args.runtimeType != Null) {
    args.keys.forEach((arg) {
      arguments.add("${arg}=${args[arg]}");
    });
  }
  argString += arguments.join("&");
  var response;
  await http.get(apiUrl + endpoint + argString)
    .then((res) {
      response = res;
    });
  return { "body": jsonDecode(response.body), "status": response.statusCode };
}

Future apiPost(String endpoint, Map args) async {
  var response;
  args["app"] = "3";
  await http.post(apiUrl + endpoint, body: args)
    .then((res) {
      response = res;
    });
  return { "body": jsonDecode(response.body), "status": response.statusCode };
}

Future<int> getUserId(String username) async {
  var request = await apiGet("get-user-id", { "username": username });
  return request["body"]["user_id"];
}

Future<Session> login(String username, String password) async {
  var request = await apiPost("users/auth-token", { "username": username, "password": password });
  if (request["body"]["success"]) {
    return Session(request["body"]["auth_token"]);
  } else {
    throw ArgumentError(request["body"]["error"]);
  }
}

Future<User> getUserData(int uid) async {
  var request = await apiGet("users/$uid");
  if (request["body"]["success"]) {
    return User(request["body"]["profile"], uid);
  } else {
    throw ArgumentError(request["body"]["error"]);
  }
}