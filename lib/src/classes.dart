class Rant {
  int id, score;
  String text, username;
  List<String> tags, comments;

  Rant(Map json) {
    id = json["id"];
    text = json["text"];
    score = json["score"];
    username = json["user_username"];
    tags = json["tags"];
  }
}

class User {
  String username, about, location, skills, website, gitHub;
  int score, id;

  User(Map json, this.id) {
    username = json["username"];
    about = json["about"];
    location = json["location"];
    skills = json["skills"];
    website = json["website"];
    gitHub = json["github"];
    score = json["score"];
  }
}

class Comment {
  int id, rant_id, score, vote_state, user_id;
  String body, user_username;

  User (Map json) {
    id = json["id"];
    rant_id = json["rant_id"];
    score = json["score"];
    vote_state = json["vote_state"];
    user_id = json["user_id"];
    body = json["body"];
    user_username = json["user_username"];
  }
}

class Session {
  int id, expire_time, user_id;
  String key;

  Session (Map json) {
    id = json["id"];
    expire_time = json["expire_time"];
    user_id = json["user_id"];
    key = json["key"];
  }
}